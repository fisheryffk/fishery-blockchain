from typing import KeysView, Generator

SERVICES_FOR_GROUP = {
    "all": "fishery_harvester fishery_timelord_launcher fishery_timelord fishery_farmer fishery_full_node fishery_wallet".split(),
    "node": "fishery_full_node".split(),
    "harvester": "fishery_harvester".split(),
    "farmer": "fishery_harvester fishery_farmer fishery_full_node fishery_wallet".split(),
    "farmer-no-wallet": "fishery_harvester fishery_farmer fishery_full_node".split(),
    "farmer-only": "fishery_farmer".split(),
    "timelord": "fishery_timelord_launcher fishery_timelord fishery_full_node".split(),
    "timelord-only": "fishery_timelord".split(),
    "timelord-launcher-only": "fishery_timelord_launcher".split(),
    "wallet": "fishery_wallet fishery_full_node".split(),
    "wallet-only": "fishery_wallet".split(),
    "introducer": "fishery_introducer".split(),
    "simulator": "fishery_full_node_simulator".split(),
}


def all_groups() -> KeysView[str]:
    return SERVICES_FOR_GROUP.keys()


def services_for_groups(groups) -> Generator[str, None, None]:
    for group in groups:
        for service in SERVICES_FOR_GROUP[group]:
            yield service


def validate_service(service: str) -> bool:
    return any(service in _ for _ in SERVICES_FOR_GROUP.values())
