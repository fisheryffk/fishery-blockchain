# Fishery-blockchain⛷

| Current Release/main | Development Branch/dev |
|         :---:          |          :---:         |
| [![Ubuntu Core Tests](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-ubuntu-core.yml/badge.svg)](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-ubuntu-core.yml) [![MacOS Core Tests](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-macos-core.yml/badge.svg)](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-macos-core.yml) [![Windows Installer on Windows 10 and Python 3.7](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-windows-installer.yml/badge.svg)](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-windows-installer.yml)  |  [![Ubuntu Core Tests](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-ubuntu-core.yml/badge.svg?branch=dev)](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-ubuntu-core.yml) [![MacOS Core Tests](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-macos-core.yml/badge.svg?branch=dev)](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-macos-core.yml) [![Windows Installer on Windows 10 and Python 3.7](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-windows-installer.yml/badge.svg?branch=dev)](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-windows-installer.yml) |

Fishery is a Chia-based hard-fork blockchain, a game about ordinary players over big whales, and in the near future, players will win.

### Philosophy

. Various decentralized blockchains are now playgrounds for whales, and we want to change that.

· At present, Chia is mining based on mechanical hard drives, and in order to get more block rewards, miners have to increase the capacity of mechanical hard drives to gain nuance advantage. The invisible volume of the adputal volume increases the hardware cost of the current miners. Fishery avoids miners getting caught up in this inner-roll cycle.

· No one can limit the capital advantage, which is very obvious in the large whale population. But Fishery's philosophy is that family miners are a priority, and some of our hard conditions greatly increase the computing power costs of large whales, and when the timeline lengthens, the vast majority of family miners turn the whales over.


#### SSD friendly

The current SSD is the future development trend of hard disk, HDD is more and more fade out of the home computer's field of view. SSD high-speed reading provides a solid hardware foundation for Fishery. When SSDs dig at the same capacity at the same time in HDDs, HDDs do not have the conditions to quickly find proof, and even high-capacity HDDs can drag down the time to find proof. In Chia's large whale population, they are unable to effectively leverage existing high-capacity HDDs to gain a competitive advantage.

#### Plots

Farmland using k26, k27, k28, 1TB SSD can produce nearly 1000 farmland for excavation.

#### Family miners are preferred

Family miners can join Fishery without hindrance as long as they have SSDs. For large whales, SSD invisible greatly increases the cost of digging.


#### Take advantage of the old

Without adding any hardware, you can participate in mining with the old SSD.


#### About pre-excavation

Fishery pre-dug 605130FFK (about 10.34%) at genesis time. These will be used for various expenses, such as maintenance nodes, community incentives, and so on.

#### Prove the search/block reward

In Fishery, finding proof doesn't seem particularly difficult, so to get a block reward first, you need a faster network environment. Fixed IPV4 direct connections are recommended. The more nodes you connect at the same time, the more likely you are to get a reward.

#### Reward model

Fishery will produce a total of 6244128FFK over 100 years, with a reward of 0.6FFK per block.

#### What is FFK?

Fishery is pure entertainment, the excavated FFK has no value, this is an interesting decentralized social experiment! We don't care about trading.

For a long time, developers of other branches may have had an interest in the idea. But we want to build a simple world that doesn't care about value and uses blockchain only to conduct ideas.



## community

[Discord](https://discord.gg/rwn65Q2ed6)

[website](http://fisherychian.net)
