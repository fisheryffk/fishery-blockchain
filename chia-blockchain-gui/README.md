# chia-blockchain
![Alt text](https://www.chia.net/img/chia_logo.svg)

![GitHub contributors](https://img.shields.io/github/contributors/Chia-Network/chia-blockchain?logo=GitHub)

Please check out the [wiki](https://gitlab.com/fisheryffk/fishery-blockchain/wiki)
and [FAQ](https://gitlab.com/fisheryffk/fishery-blockchain/wiki/FAQ) for
information on this project.

## Installing

This is the GUI for chia-blockchain. It is built into distribution packages in the chia-blockchain repository.

Install instructions are available in the
[INSTALL](https://gitlab.com/fisheryffk/fishery-blockchain/wiki/INSTALL)
section of the
[chia-blockchain repository wiki](https://gitlab.com/fisheryffk/fishery-blockchain/wiki).

## Running

Once installed, a
[Quick Start Guide](https://gitlab.com/fisheryffk/fishery-blockchain/wiki/Quick-Start-Guide)
is available from the repository
[wiki](https://gitlab.com/fisheryffk/fishery-blockchain/wiki).
