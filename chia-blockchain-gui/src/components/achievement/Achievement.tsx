import React from 'react';
import styled from 'styled-components';
import { Trans } from '@lingui/macro';
import { useHistory } from 'react-router-dom';
import { Button, Grid, Typography } from '@material-ui/core';
import LayoutMain from '../layout/LayoutMain';
import { CardHero } from '@chia/core';
import { Pool as PoolIcon } from '@chia/icons';
import useOpenExternal from '../../hooks/useOpenExternal';

const StyledPoolIcon = styled(PoolIcon)`
  font-size: 4rem;
`;

export default function Achievement() {
  const history = useHistory();
  const openExternal = useOpenExternal();

  function handleJoinPool() {
    // history.push('/dashboard/pool/add');
    openExternal('http://www.fisherychian.net/');
  }

  return (
    <LayoutMain title={<Trans>Achievement</Trans>}>
      <Grid container>
        <Grid xs={12} md={6} lg={5} item>
          <CardHero>
            {/* <StyledPoolIcon color="primary" /> */}
            <Typography variant="body1">
              <Trans>
                FFK will be able to redeem the Chain Medal (NFT) to reward players participating in this game, and if successful, this feature will be updated in the near future.
              </Trans>
            </Typography>
            <Button onClick={handleJoinPool} variant="contained" color="primary">
              <Trans>Fishery!</Trans>
            </Button>
          </CardHero>
        </Grid>



      </Grid>
    </LayoutMain>
  );
}
