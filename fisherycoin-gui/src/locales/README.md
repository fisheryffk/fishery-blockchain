# Localization

Thanks for helping to translate the GUI for fisherycoin.

Please head over to our [Crowdin project](https://crowdin.com/project/fisherycoin/) and add/edit translations there.
