import React from 'react';
import { SvgIcon, SvgIconProps } from '@material-ui/core';
import { ReactComponent as fisheryIcon } from './images/fishery.svg';

export default function Keys(props: SvgIconProps) {
  return <SvgIcon component={fisheryIcon} viewBox="0 0 1024 1024" {...props} />;
}
