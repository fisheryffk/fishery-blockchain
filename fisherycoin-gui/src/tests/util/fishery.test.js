const fishery = require('../../util/fishery');

describe('fishery', () => {
  it('converts number mojo to fishery', () => {
    const result = fishery.mojo_to_fishery(1000000);

    expect(result).toBe(0.000001);
  });
  it('converts string mojo to fishery', () => {
    const result = fishery.mojo_to_fishery('1000000');

    expect(result).toBe(0.000001);
  });
  it('converts number mojo to fishery string', () => {
    const result = fishery.mojo_to_fishery_string(1000000);

    expect(result).toBe('0.000001');
  });
  it('converts string mojo to fishery string', () => {
    const result = fishery.mojo_to_fishery_string('1000000');

    expect(result).toBe('0.000001');
  });
  it('converts number fishery to mojo', () => {
    const result = fishery.fishery_to_mojo(0.000001);

    expect(result).toBe(1000000);
  });
  it('converts string fishery to mojo', () => {
    const result = fishery.fishery_to_mojo('0.000001');

    expect(result).toBe(1000000);
  });
  it('converts number mojo to colouredcoin', () => {
    const result = fishery.mojo_to_colouredcoin(1000000);

    expect(result).toBe(1000);
  });
  it('converts string mojo to colouredcoin', () => {
    const result = fishery.mojo_to_colouredcoin('1000000');

    expect(result).toBe(1000);
  });
  it('converts number mojo to colouredcoin string', () => {
    const result = fishery.mojo_to_colouredcoin_string(1000000);

    expect(result).toBe('1,000');
  });
  it('converts string mojo to colouredcoin string', () => {
    const result = fishery.mojo_to_colouredcoin_string('1000000');

    expect(result).toBe('1,000');
  });
  it('converts number colouredcoin to mojo', () => {
    const result = fishery.colouredcoin_to_mojo(1000);

    expect(result).toBe(1000000);
  });
  it('converts string colouredcoin to mojo', () => {
    const result = fishery.colouredcoin_to_mojo('1000');

    expect(result).toBe(1000000);
  });
});
