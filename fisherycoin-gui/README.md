# fisherycoin
![Alt text](https://www.fisherychian.net/img/fishery_logo.svg)

![GitHub contributors](https://img.shields.io/github/contributors/fishery-Network/fisherycoin?logo=GitHub)

Please check out the [wiki](https://github.com/fisheryffk/Fishery-blockchain/wiki)
and [FAQ](https://github.com/fisheryffk/Fishery-blockchain/wiki/FAQ) for
information on this project.

## Installing

This is the GUI for fisherycoin. It is built into distribution packages in the fisherycoin repository.

Install instructions are available in the
[INSTALL](https://github.com/fisheryffk/Fishery-blockchain/wiki/INSTALL)
section of the
[fisherycoin repository wiki](https://github.com/fisheryffk/Fishery-blockchain/wiki).

## Running

Once installed, a
[Quick Start Guide](https://github.com/fisheryffk/Fishery-blockchain/wiki/Quick-Start-Guide)
is available from the repository
[wiki](https://github.com/fisheryffk/Fishery-blockchain/wiki).
