# Installation

Install instructions have been moved to the [INSTALL](https://github.com/fisheryffk/Fishery-blockchain/wiki/INSTALL) section of the repository [Wiki](https://github.com/fisheryffk/Fishery-blockchain/wiki/wiki).

After installing, follow the remaining instructions in the
[Quick Start Guide](https://github.com/fisheryffk/Fishery-blockchain/wiki/Quick-Start-Guide)
to run the software.
