# Installation

Install instructions have been moved to the [INSTALL](https://gitlab.com/fisheryffk/fishery-blockchain/wiki/INSTALL) section of the repository [Wiki](https://gitlab.com/fisheryffk/fishery-blockchain/wiki).

After installing, follow the remaining instructions in the
[Quick Start Guide](https://gitlab.com/fisheryffk/fishery-blockchain/wiki/Quick-Start-Guide)
to run the software.
