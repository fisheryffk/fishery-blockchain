# Fishery-blockchain ⛷

| Current Release/main | Development Branch/dev |
|         :---:          |          :---:         |
| [![Ubuntu Core Tests](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-ubuntu-core.yml/badge.svg)](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-ubuntu-core.yml) [![MacOS Core Tests](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-macos-core.yml/badge.svg)](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-macos-core.yml) [![Windows Installer on Windows 10 and Python 3.7](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-windows-installer.yml/badge.svg)](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-windows-installer.yml)  |  [![Ubuntu Core Tests](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-ubuntu-core.yml/badge.svg?branch=dev)](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-ubuntu-core.yml) [![MacOS Core Tests](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-macos-core.yml/badge.svg?branch=dev)](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-test-macos-core.yml) [![Windows Installer on Windows 10 and Python 3.7](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-windows-installer.yml/badge.svg?branch=dev)](https://github.com/Chia-Network/chia-blockchain/actions/workflows/build-windows-installer.yml) |

Fishery是一个基于Chia的硬分叉区块链，这是一场关于普通玩家战胜大鲸鱼的游戏，不久的将来，玩家必将赢得胜利。

#### 理念

. 各种去中心化的区块链现在都成了大鲸鱼的游乐场，我们想要改变这种现象。

· 当前Chia基于机械硬盘挖矿，广大矿工为了获得更多区块奖励，而不得不增加机械硬盘容量来获得算力优势。算力内卷无形增加了当前矿工的硬件成本。Fishery避免矿工们陷入这种内卷循环。

· 没有任何人能限制资本优势，在大鲸鱼用群体中，资本优势极为明显，而Fishery的理念是家庭矿工优先，我们一些硬性条件极大的增加大鲸鱼的算力成本，当时间线拉长时，广大的家庭矿工会将大鲸鱼干翻。

#### SSD友好

当前SSD为未来硬盘发展趋势，HDD越发淡出家庭电脑的视野。SSD高速读取为Fishery奠定了坚实的硬件基础。在同等容量下SSD于HDD同时进行挖掘时，HDD不具备快速寻找证明的条件，甚至大容量的HDD反而会拖累寻找证明的时间。在Chia的大鲸鱼群体中，他们无法有效利用现有的大容量HDD来取得竞争优势。

#### 农田

农田使用k26、k27、k28，1TB的SSD就能产生近1000个农田进行挖掘。

#### 家庭矿工优先

家庭矿工只要有SSD就可以无阻碍的加入Fishery。对于大鲸鱼而言，SSD无形极大增加了挖掘成本。


#### 利旧

无需增加任何硬件设备，利用老旧的SSD就能参与挖掘。


#### 关于预挖

Fishery在创世时预挖了605130FFK（约10.34%）。这些将用于各项支出，如：维护节点、社区奖励等。

#### 证明寻找/块奖励

在Fishery中，寻找证明似乎不是一件特别困难的事情，所以为了能抢先获得区块奖励，你需要更快的网络环境。建议使用固定IPV4直连。同时连接的节点越多也能增加抢到奖励的概率。

#### 奖励模型

Fishery在100年将共共产出6244128FFK，每个区块奖励为0.6FFK。

#### FFK是什么?

Fishery纯属娱乐，挖掘出来的FFK没有任何价值，这是一次有趣的去中心化社会实验! 我们不关注交易。

长时间以来，其他分支的开发者们或许都有利益的想法。但我们想建立一个单纯的世界，不关心价值，只用区块链来传导理念。



## 社区

[Discord](https://discord.gg/rwn65Q2ed6)

[website](http://fisherychian.net)
